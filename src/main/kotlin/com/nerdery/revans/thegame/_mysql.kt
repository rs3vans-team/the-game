package com.nerdery.revans.thegame

import java.io.File
import java.io.FileInputStream
import java.sql.Connection
import java.sql.DriverManager
import java.util.*

private const val DB_CONFIG_FILE = "./private/db.properties"

val dbProperties: Properties = Properties().apply {
    load(FileInputStream(File(DB_CONFIG_FILE)))
}

val dbConnection: Connection = try {
    Class.forName(dbProperties.getProperty("db.driver"))
    DriverManager.getConnection(
            dbProperties.getProperty("db.url"),
            dbProperties.getProperty("db.user"),
            dbProperties.getProperty("db.password")
    )
} catch (e: Exception) {
    throw IllegalStateException("Could not establish a connection to the database!", e)
}