package com.nerdery.revans.thegame

import java.math.BigInteger
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock

object ItemSequences : TheGameThread() {

    private val lock = ReentrantLock()
    private val wakeCondition = lock.newCondition()
    private val shouldRun = AtomicBoolean(false)

    override fun tick() = lock.withLock {
        while (!shouldRun.get())
            wakeCondition.await()

        if (Items.strategyIsActive("pushOverLimit"))
            pushOverLimit()

        shouldRun.set(false)
    }

    fun triggerAutomaticItemSequences() = lock.withLock {
        if (shouldRun.compareAndSet(false, true))
            wakeCondition.signalAll()
    }

    /**
     * Push someone who's "unguarded" over the point limit, getting them grounded...
     */
    fun pushOverLimit() {
        LeaderBoard.unguardedEnemies.forEach {
            val (player, points) = it
            val swords = numMasterSwordsToTakeOverLimit(points)
            if (swords > 0 && swords < 10) {
                Items.rapidFireItem("Master Sword", target = player, times = swords)
            }
        }
    }

    private fun numMasterSwordsToTakeOverLimit(points: BigInteger): Int {
        if (points < BigInteger.ZERO)
            return 0

        var swordsAvailable = Items.items["Master Sword"]?.size ?: 0
        var swords = 0
        var nextPoints = points

        while (nextPoints <= maxPointsAllowed && swordsAvailable > 0) {
            nextPoints = pointsAfterMasterSword(nextPoints)
            swordsAvailable--
            swords++
        }

        return if (nextPoints > maxPointsAllowed) swords else 0
    }

    private fun pointsAfterMasterSword(points: BigInteger) =
            (masterSwordMultiplier * points.toBigDec()).toBigInt()

    fun getToTheTop() {
        Points.stop()
        Items.queueItem("Star", target = ME, hold = true)
        Items.rapidFireItem("Vanellope", target = ME) {
            Items.releaseHeldItems()
            Points.checkPointsAndEffects()
            Points.start()
        }
    }
}