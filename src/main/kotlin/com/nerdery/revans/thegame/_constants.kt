package com.nerdery.revans.thegame

import java.math.BigDecimal

const val BASE_URL = "http://thegame.nerderylabs.com:1337"
const val POINTS_URL = "$BASE_URL/points"
const val ITEMS_URL = "$BASE_URL/items/use"

const val ME = "revans"

const val MILLIS_PER_MINUTE = 60000L
const val MILLIS_PER_FIFTEEN_SECONDS = 15000L

val maxPointsAllowed = "1337133713370".toBigInt()
val targetWinningPoints = maxPointsAllowed - "100000".toBigInt()
val oneTrillionPoints = "1000000000000".toBigInt()

val masterSwordMultiplier = BigDecimal.valueOf(1 / 0.75)!!
