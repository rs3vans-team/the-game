package com.nerdery.revans.thegame

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import java.io.File

val myApiKey = File("private/apiKey").readText()

val otherApiKeys: Map<String, String> = File("private/otherApiKeys")
        .readText()
        .split("\n")
        .filter { !it.startsWith("//") }
        .associate { it.trim().split("|").let { it[0] to it[1] } }

@SpringBootApplication
open class TheGameBot

fun main(args: Array<String>) {
    val env = System.getenv()

    if (env["START_POINTS"]?.toBoolean() ?: true)
        Points.start()
    if (env["START_ITEMS"]?.toBoolean() ?: true)
        Items.start()
    if (env["START_ITEM_SEQUENCES"]?.toBoolean() ?: true)
        ItemSequences.start()
    if (env["START_LEADER_BOARD"]?.toBoolean() ?: true)
        LeaderBoard.start()
    if (env["START_EVENTS"]?.toBoolean() ?: true)
        Events.start()

    if (env["START_FARMERS"]?.toBoolean() ?: true)
        Farmer.startAll()
    if (env["START_ATTACKERS"]?.toBoolean() ?: true)
        Attacker.startAll()

    SpringApplication.run(TheGameBot::class.java)
}