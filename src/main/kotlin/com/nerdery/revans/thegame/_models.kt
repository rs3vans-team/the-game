package com.nerdery.revans.thegame

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class PointsModel @JsonCreator constructor(
        @JsonProperty("Messages") val messages: List<String>,
        @JsonProperty("Points") val points: String,
        @JsonProperty("Item") val item: ItemWrapper?,
        @JsonProperty("Effects") val effects: Set<String>)

@JsonIgnoreProperties(ignoreUnknown = true)
data class ItemWrapper @JsonCreator constructor(@JsonProperty("Fields") val wrapped: List<ItemModel>)

@JsonIgnoreProperties(ignoreUnknown = true)
data class ItemModel @JsonCreator constructor(
        @JsonProperty("Id") val id: String,
        @JsonProperty("Name") val name: String,
        @JsonProperty("Rarity") val rarity: Int,
        @JsonProperty("Description") val description: String)

@JsonIgnoreProperties(ignoreUnknown = true)
data class ItemUseModel @JsonCreator constructor(
        @JsonProperty("Messages") val messages: List<String>,
        @JsonProperty("TargetName") val targetName: String,
        @JsonProperty("Points") val points: String)

@JsonIgnoreProperties(ignoreUnknown = true)
data class PlayerModel @JsonCreator constructor(
        @JsonProperty("PlayerName") val playerName: String,
        @JsonProperty("Points") val points: String,
        @JsonProperty("Title") val title: String?,
        @JsonProperty("Effects") val effects: Set<String>,
        @JsonProperty("Badges") val badges: Set<BadgeModel>)

@JsonIgnoreProperties(ignoreUnknown = true)
data class WrappedPlayerModel @JsonCreator constructor(
        @JsonProperty("Fields") val wrapped: List<PlayerModel>)

@JsonIgnoreProperties(ignoreUnknown = true)
data class BadgeModel @JsonCreator constructor(
        @JsonProperty("BadgeName") val name: String
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class EffectsModel @JsonCreator constructor(
        @JsonProperty("Timestamp") val timestamp: String,
        @JsonProperty("Creator") val creator: String,
        @JsonProperty("Targets") val target: String,
        @JsonProperty("Effect") val effect: EffectModel)

@JsonIgnoreProperties(ignoreUnknown = true)
data class EffectModel @JsonCreator constructor(
        @JsonProperty("EffectName") val name: String,
        @JsonProperty("EffectType") val type: String,
        @JsonProperty("Duration") val duration: DurationModel,
        @JsonProperty("VoteGain") val voteGain: String)

@JsonIgnoreProperties(ignoreUnknown = true)
data class DurationModel @JsonCreator constructor(
        @JsonProperty("Case") val type: String
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class DurationFieldsModel @JsonCreator constructor(
        @JsonProperty("Fields") val fields: List<DurationFieldModel>
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class DurationFieldModel @JsonCreator constructor(
        @JsonProperty("Case") val field: String,
        @JsonProperty("Fields") val meta: List<String>
)