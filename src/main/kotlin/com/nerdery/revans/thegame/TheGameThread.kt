package com.nerdery.revans.thegame

import java.util.concurrent.TimeUnit
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock

abstract class TheGameThread : Runnable {

    var running: Boolean = false
        private set

    private val lock = ReentrantLock()
    private val sleepCondition = lock.newCondition()
    private val dieCondition = lock.newCondition()

    private var thread: Thread? = null

    override fun run() = lock.withLock {
        threadLogger.info("the ${this.javaClass.simpleName} is starting")

        initialTimeToWait()
                .millisToNanos()
                .doWait()

        while (running) {
            val start = System.nanoTime()
            val failure = try {
                tick()
                false
            } catch (e: Exception) {
                threadLogger.error("the ${this.javaClass.simpleName} has failed", e)
                true
            }
            if (running)
                timeToWait((System.nanoTime() - start).nanosToMillis(), failure)
                        .millisToNanos()
                        .doWait()
        }

        threadLogger.info("the ${this.javaClass.simpleName} has finished")
        dieCondition.signalAll()
    }

    private fun Long.doWait() {
        var waitLeft = this
        while (waitLeft > 0)
            waitLeft = sleepCondition.awaitNanos(waitLeft)
    }

    protected open fun initialTimeToWait(): Long = 0L
    protected open fun timeToWait(timeWorked: Long, failure: Boolean): Long = 0L

    protected abstract fun tick()

    fun start(): Boolean = lock.withLock {
        if (!running) {
            running = true
            thread = Thread(this).apply { start() }
            return true
        }
        return false
    }

    fun stop(): Boolean = lock.withLock {
        if (running) {
            running = false
            if (thread != Thread.currentThread()) {
                sleepCondition.signalAll()
                dieCondition.await()
            }
            thread = null
            return true
        }
        return false
    }

    companion object {

        fun Long.millisToNanos(): Long = TimeUnit.MILLISECONDS.toNanos(this)
        fun Long.nanosToMillis(): Long = TimeUnit.NANOSECONDS.toMillis(this)
    }
}