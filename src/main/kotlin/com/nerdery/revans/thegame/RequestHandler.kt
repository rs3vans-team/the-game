package com.nerdery.revans.thegame

import java.util.concurrent.CompletableFuture
import java.util.concurrent.Future
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.TimeUnit

class RequestHandler(val apiKey: String? = null) : TheGameThread() {

    private val queue = LinkedBlockingQueue<QueuedRequest>()

    override fun timeToWait(timeWorked: Long, failure: Boolean): Long = 300L

    override fun tick() {
        val (url, method, apiKey, future) = queue.poll(1500L, TimeUnit.MILLISECONDS) ?: return
        future.complete(executeAuthedHttpRequest(url, method, apiKey))
    }

    fun sendMessage(url: String, method: HttpMethod): Future<String?> =
            CompletableFuture<String?>().apply { queue.offer(QueuedRequest(url, method, apiKey, this)) }

    private data class QueuedRequest(
            val url: String,
            val method: HttpMethod,
            val apiKey: String?,
            val future: CompletableFuture<String?>)

    companion object {

        val default = RequestHandler().apply { start() }

        private val handlers = mutableMapOf<String, RequestHandler>()

        fun forKey(apiKey: String?) = if (apiKey == null) {
            default
        } else {
            handlers.getOrPut(apiKey) { RequestHandler(apiKey).apply { start() } }
        }

        fun sendMessage(url: String, method: HttpMethod): Future<String?> = default.sendMessage(url, method)
    }
}