package com.nerdery.revans.thegame

import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.TimeUnit
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock

class Attacker(val apiKey: String) : TheGameThread() {

    var playerName: String
        private set

    private var itemLastUsedOn: Long = System.currentTimeMillis()

    init {
        val playerData = RequestHandler.forKey(apiKey).sendMessage(POINTS_URL, HttpMethod.GET).get()?.let {
            parseJson<WrappedPlayerModel>(it)?.wrapped?.first()
        }

        if (playerData == null) {
            playerName = "<bad API key>"
        } else {
            playerName = playerData.playerName
            LeaderBoard.updateEffectsForName(playerData.playerName, playerData.effects)
        }
    }

    override fun initialTimeToWait(): Long = timeToWait(0L, false)
    override fun timeToWait(timeWorked: Long, failure: Boolean): Long {
        val wait = if (LeaderBoard.isVampire) {
            MILLIS_PER_FIFTEEN_SECONDS // if you're a vampire, feel free to use items more often - go ahead and limit it to 15s tho...
        } else {
            MILLIS_PER_MINUTE
        }
        return if ((System.currentTimeMillis() - itemLastUsedOn) > wait)
            0L
        else wait
    }

    override fun tick() {
        val callback = queue.poll(1500L, TimeUnit.MILLISECONDS) ?: return
        callback.invoke(apiKey)
    }

    companion object {

        private val attackers = otherApiKeys.values
                .map { Attacker(it) }
                .filter { it.playerName != "<bad API key>" }

        private val queue = LinkedBlockingQueue<(String?) -> Unit>()

        private val lock = ReentrantLock()

        fun startAll() = lock.withLock { attackers.forEach { it.start() } }
        fun stopAll() = lock.withLock {
            attackers.forEach { it.stop() }
            while (queue.isNotEmpty()) {
                queue.take().invoke(null)
            }
        }

        fun dispatch(callback: (String?) -> Unit) = lock.withLock { queue.offer(callback) }
    }
}