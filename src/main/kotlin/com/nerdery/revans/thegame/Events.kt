package com.nerdery.revans.thegame

import java.math.BigInteger
import java.time.LocalDateTime
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock

object Events : TheGameThread() {

    val events: List<EffectEvent>
        get() = lock.withLock { effectEvents.toList() }

    private val timestampParser = DateTimeFormatter.ISO_OFFSET_DATE_TIME

    private val effectEvents = BoundedLinkedList<EffectEvent>(1000)

    private val lock = ReentrantLock()

    override fun timeToWait(timeWorked: Long, failure: Boolean): Long = 10000L

    override fun tick() = lock.withLock {
        val mostRecentEvent = effectEvents.firstOrNull()?.timestamp

        val newEvents = requestEvents()
                .takeWhile { !(mostRecentEvent?.equals(it.timestamp) ?: false) }

        if (mostRecentEvent == null) {
            val lastUsedItems = mutableMapOf<String, Long>()
            newEvents.forEach {
                if (it.creator == ME) {
                    lastUsedItems[it.effect.name] = ZonedDateTime.parse(it.timestamp, timestampParser).toInstant().toEpochMilli()
                }
            }
            if (lastUsedItems.isNotEmpty())
                Items.recordItemsUsed(lastUsedItems)
        }

        effectEvents.boundedAddAll(newEvents.map {
            with(it) {
                EffectEvent(
                        timestamp,
                        effect.name,
                        effect.type,
                        creator,
                        target,
                        BigInteger(effect.voteGain)
                )
            }
        })
    }


    private fun requestEvents(): List<EffectsModel> =
            executeHttpRequestAndParseJson<List<EffectsModel>>("$BASE_URL/effects", HttpMethod.GET) ?: emptyList()

    private fun requestEventsForName(name: String): List<EffectsModel> =
            executeHttpRequestAndParseJson<List<EffectsModel>>("$BASE_URL/effects/$name", HttpMethod.GET) ?: emptyList()
}
