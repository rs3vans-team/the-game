package com.nerdery.revans.thegame

import java.math.BigInteger

enum class UseType { BUFF, ATTACK, UNKNOWN }
enum class EffectType { INSTANT, PERSISTENT, UNKNOWN }
enum class AutoTargetType { NONE, SELF, FRONT, BEHIND, FIRST, TARGET_AND_SELF }

data class ItemType(
        val itemName: String,
        val itemDescription: String,
        val itemRarity: Int,
        val useType: UseType,
        val effectType: EffectType,
        val autoTargetType: AutoTargetType) {

    constructor(itemName: String,
                itemDescription: String,
                itemRarity: Int) : this(
            itemName,
            itemDescription,
            itemRarity,
            UseType.UNKNOWN,
            EffectType.UNKNOWN,
            AutoTargetType.NONE
    )
}

data class Item(
        val id: String,
        val type: ItemType,
        val source: String
)

data class EffectEvent(
        val timestamp: String,
        val name: String,
        val type: String,
        val creator: String,
        val target: String,
        val pointsChange: BigInteger
)