package com.nerdery.revans.thegame

import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*
import java.time.Instant
import java.time.ZoneId
import java.time.ZonedDateTime
import java.util.*

@RestController
class Controller {

    private val threads = mapOf(
            "Points" to Points,
            "Items" to Items,
            "ItemSequences" to ItemSequences,
            "LeaderBoard" to LeaderBoard,
            "Events" to Events
    )

    @RequestMapping("threads",
            method = arrayOf(RequestMethod.GET),
            produces = arrayOf(MediaType.APPLICATION_JSON_VALUE))
    fun threadStatus(): Map<String, Boolean> = threads
            .mapValues { it.value.running }

    @RequestMapping("threads/{thread}/start",
            method = arrayOf(RequestMethod.PUT),
            produces = arrayOf(MediaType.APPLICATION_JSON_VALUE))
    fun startThread(@PathVariable("thread") thread: String): Boolean = threads[thread.trim()]?.start() ?: false

    @RequestMapping("threads/{thread}/stop",
            method = arrayOf(RequestMethod.PUT),
            produces = arrayOf(MediaType.APPLICATION_JSON_VALUE))
    fun stopThread(@PathVariable("thread") thread: String): Boolean = threads[thread.trim()]?.stop() ?: false

    @RequestMapping("me",
            method = arrayOf(RequestMethod.GET),
            produces = arrayOf(MediaType.APPLICATION_JSON_VALUE))
    fun myStatus(): Map<String, Any> = mapOf(
            "points" to Points.points,
            "title" to (LeaderBoard.myTitle ?: ""),
            "effects" to LeaderBoard.myEffects,
            "badges" to LeaderBoard.myBadges,
            "pointEvents" to Points.pointEvents,
            "recentlyUsedItems" to Items.usedItems,
            "recentlyAcquiredItems" to Items.acquiredItems
    )

    @RequestMapping("messages",
            method = arrayOf(RequestMethod.GET),
            produces = arrayOf(MediaType.APPLICATION_JSON_VALUE))
    fun listMessages(): List<String> = Points.messages

    @RequestMapping("points",
            method = arrayOf(RequestMethod.GET),
            produces = arrayOf(MediaType.APPLICATION_JSON_VALUE))
    fun listPoints(): Map<String, String> =
            LeaderBoard.points.associateTo(LinkedHashMap<String, String>()) { it.first to it.second.toString() }

    @RequestMapping("events",
            method = arrayOf(RequestMethod.GET),
            produces = arrayOf(MediaType.APPLICATION_JSON_VALUE))
    fun listEvents(): List<EffectEvent> = Events.events

    @RequestMapping("events/me",
            method = arrayOf(RequestMethod.GET),
            produces = arrayOf(MediaType.APPLICATION_JSON_VALUE))
    fun listEventsForMe(): List<EffectEvent> = listEventsForPlayer(ME)

    @RequestMapping("events/{playerName}",
            method = arrayOf(RequestMethod.GET),
            produces = arrayOf(MediaType.APPLICATION_JSON_VALUE))
    fun listEventsForPlayer(@PathVariable("playerName") playerName: String): List<EffectEvent> =
            Events.events.filter { playerName.trim().let { name -> it.creator == name || it.target == name } }

    @RequestMapping("items",
            method = arrayOf(RequestMethod.GET),
            produces = arrayOf(MediaType.APPLICATION_JSON_VALUE))
    fun summarizeItems(): Map<String, Int> =
            Items.items.mapValues { it.value.size }.toSortedMap()

    @RequestMapping("items/strategies",
            method = arrayOf(RequestMethod.GET),
            produces = arrayOf(MediaType.APPLICATION_JSON_VALUE))
    fun listItemStrategies(): Map<String, Boolean> = Items.strategies

    @RequestMapping("items/strategies/{strategy}",
            method = arrayOf(RequestMethod.PUT))
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun setItemStrategy(@PathVariable("strategy") strategy: String,
                        @RequestParam("value") value: Boolean) {
        Items.activateStrategy(strategy, value)
    }

    @RequestMapping("items/types",
            method = arrayOf(RequestMethod.GET),
            produces = arrayOf(MediaType.APPLICATION_JSON_VALUE))
    fun listItemTypes(): Map<String, ItemType> = Items.itemTypes

    @RequestMapping("items/lastUsed",
            method = arrayOf(RequestMethod.GET),
            produces = arrayOf(MediaType.APPLICATION_JSON_VALUE))
    fun listItemsLastUsed(): Map<String, String> = Items.itemsLastUsed
            .mapValues { ZonedDateTime.ofInstant(Instant.ofEpochMilli(it.value), ZoneId.systemDefault()).toString() }

    @RequestMapping("items/take/{item}",
            method = arrayOf(RequestMethod.POST),
            produces = arrayOf(MediaType.TEXT_PLAIN_VALUE))
    fun takeItem(@PathVariable("item") item: String): String? = Items.takeItem(item)?.id

    @RequestMapping("items/queue",
            method = arrayOf(RequestMethod.GET),
            produces = arrayOf(MediaType.APPLICATION_JSON_VALUE))
    fun listQueuedItems(): Map<String, String> =
            Items.queue.associate { "${it.first.id} - ${it.first.type.itemName}" to it.second }

    @RequestMapping("items/queue/{item}",
            method = arrayOf(RequestMethod.POST),
            produces = arrayOf(MediaType.APPLICATION_JSON_VALUE))
    fun queueItem(@PathVariable("item") item: String,
                  @RequestParam("target", required = false) target: String?,
                  @RequestParam("times", required = false) times: Int?): Boolean =
            Items.queueItem(item.trim(), target?.trim(), times)

    @RequestMapping("items/queue",
            method = arrayOf(RequestMethod.DELETE))
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun clearItemQueue() = Items.clearItemQueue()

    @RequestMapping("items/fire/{item}",
            method = arrayOf(RequestMethod.POST),
            produces = arrayOf(MediaType.APPLICATION_JSON_VALUE))
    fun rapidFireItems(@PathVariable("item") item: String,
                       @RequestParam("target", required = false) target: String?,
                       @RequestParam("times", required = false) times: Int?) =
            Items.rapidFireItem(item.trim(), target?.trim(), times)

    @RequestMapping("items/reload",
            method = arrayOf(RequestMethod.POST))
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun reloadItemsFromDb() = Items.reloadItemsFromDb()

    @RequestMapping("getToTheTop",
            method = arrayOf(RequestMethod.POST))
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun getToTheTop() = ItemSequences.getToTheTop()
}