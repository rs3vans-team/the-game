package com.nerdery.revans.thegame

import com.nerdery.revans.thegame.LeaderBoard.isAfflictedWith
import com.nerdery.revans.thegame.LeaderBoard.isNotAfflictedWith
import java.sql.Timestamp
import java.time.LocalDateTime
import java.util.*
import java.util.concurrent.TimeUnit
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock

object Items : TheGameThread() {

    val itemTypes: Map<String, ItemType>
        get() = _itemTypes
    val items: Map<String, List<Item>>
        get() = _items
    val queue: List<Pair<Item, String>>
        get() = _queue
    val acquiredItems: List<ItemAcquiredEvent>
        get() = _acquiredItems
    val usedItems: List<ItemUsedEvent>
        get() = _usedItems
    val itemsLastUsed: Map<String, Long>
        get() = _itemsLastUsed
    val strategies: Map<String, Boolean>
        get() = _strategies

    private val bonusItemMessageRegex = Regex("^[^<]+<([^>]+)> \\| <([^>]+)>$")

    private val _itemTypes = mutableMapOf<String, ItemType>()
    private val _items = mutableMapOf<String, MutableList<Item>>()
    private val _queue = LinkedList<Pair<Item, String>>()
    private val _acquiredItems = BoundedLinkedList<ItemAcquiredEvent>(50)
    private val _usedItems = BoundedLinkedList<ItemUsedEvent>(50)
    private val _itemsLastUsed = mutableMapOf<String, Long>()
    private val _strategies: MutableMap<String, Boolean> = mutableMapOf()

    private var itemLastUsedOn: Long = System.currentTimeMillis()
    private var shouldHold = false

    private val lock = ReentrantLock()
    private val holdCondition = lock.newCondition()

    init {
        _itemTypes.putAll(loadItemTypes()
                .associateBy { it.itemName })

        _items.putAll(loadItems()
                .groupBy { it.type.itemName }
                .mapValues { LinkedList(it.value) })

        // initialize autoSelect strategy
        _strategies.put("autoSelect", System.getenv("ITEMS_AUTO_SELECT")?.toBoolean() ?: true)
        if (System.getenv("AT_THE_TOP")?.toBoolean() ?: false)
            _strategies.put("atTheTop", true)
    }

    override fun initialTimeToWait(): Long = timeToWait(0L, false)
    override fun timeToWait(timeWorked: Long, failure: Boolean): Long {
        val wait = if (LeaderBoard.isVampire) {
            MILLIS_PER_FIFTEEN_SECONDS // if you're a vampire, feel free to use items more often - go ahead and limit it to 15s tho...
        } else {
            MILLIS_PER_MINUTE
        }
        return if ((System.currentTimeMillis() - itemLastUsedOn) > wait)
            0L
        else wait
    }

    override fun tick() {
        var usedResponse: List<String>? = null
        while (usedResponse == null) {
            val nextItem = lock.withLock {
                while (shouldHold)
                    holdCondition.await()

                if (_queue.isEmpty()) {
                    if (!(_strategies["autoSelect"] ?: false)) return
                    selectItem()
                } else {
                    _queue.poll()
                }
            }
            if (nextItem != null) {
                val (item, target) = nextItem
                usedResponse = item.use(target)
            } else break
        }
        if (usedResponse != null)
            itemLastUsedOn = System.currentTimeMillis()
    }

    fun strategyIsActive(strategy: String): Boolean = lock.withLock { _strategies[strategy] ?: false }

    fun activateStrategy(strategy: String, value: Boolean) = lock.withLock {
        _strategies[strategy] = value
    }

    fun acquireItem(model: ItemModel, source: String) = lock.withLock {
        val type = _itemTypes[model.name]
                ?: ItemType(model.name, model.description, model.rarity).apply { save() }

        Item(model.id, type, source).save()

        _acquiredItems.boundedAdd(ItemAcquiredEvent(type.itemName, source))
    }

    fun queueItem(name: String, target: String? = null, times: Int? = null, hold: Boolean = false): Boolean = lock.withLock {
        val items = (1..(times ?: 1))
                .map { findItem(name, manualTarget = target) }
                .filterNotNull()

        if (hold && !shouldHold)
            setHold()

        return if (items.isNotEmpty()) {
            items.forEach { _queue.offer(it) }
            true
        } else false
    }

    fun clearItemQueue() = lock.withLock {
        _queue.forEach { it.first.add() }
        _queue.clear()
        clearHold()
    }

    fun rapidFireItem(name: String,
                      target: String? = null,
                      times: Int? = null,
                      callback: ((List<String>?) -> Unit)? = null) = lock.withLock {
        val items = (1..(times ?: 1))
                .map { findItem(name, manualTarget = target, targetableOnly = true) }
                .filterNotNull()

        if (items.isNotEmpty()) {
            items.forEach {
                val (item, itemTarget) = it
                Attacker.dispatch { apiKey: String? ->
                    val response = if (apiKey != null) {
                        item.use(itemTarget, apiKey)
                    } else {
                        item.add()
                        null
                    }
                    callback?.invoke(response)
                }
            }
        }
    }

    fun takeItem(name: String): Item? = lock.withLock {
        return _items[name]?.let {
            if (it.size == 1)
                _items.remove(name)
            it.removeAt(0)
        }
    }

    fun releaseHeldItems() = lock.withLock { clearHold() }

    private fun setHold() {
        shouldHold = true
        TimeUnit.MILLISECONDS.sleep(timeToWait(0L, false))
    }

    private fun clearHold() {
        shouldHold = false
        holdCondition.signalAll()
    }

    fun recordItemsUsed(itemsAndTimes: Map<String, Long>) = lock.withLock {
        _itemsLastUsed.putAll(itemsAndTimes)
    }

    fun reloadItemsFromDb() = lock.withLock {
        _items.clear()
        _items.putAll(loadItems()
                .groupBy { it.type.itemName }
                .mapValues { LinkedList(it.value) })
    }

    private fun findOneOfItem(types: Array<String>): Pair<Item, String>? = types
            .asSequence()
            .map { findItem(it) }
            .filterNotNull()
            .firstOrNull()

    private fun findRandomOneOfItem(types: Array<String>): Pair<Item, String>? = findOneOfItem(types.shuffle())

    private fun findItem(name: String,
                         manualTarget: String? = null,
                         ignoreCurrentEffects: Boolean = false,
                         targetableOnly: Boolean = false): Pair<Item, String>? {
        val type = _itemTypes[name] ?: return null

        if (targetableOnly && type.autoTargetType != AutoTargetType.NONE)
            return null

        val target = manualTarget ?: when (type.autoTargetType) {
            AutoTargetType.SELF -> ME
            AutoTargetType.TARGET_AND_SELF -> ME
            AutoTargetType.FIRST -> if (LeaderBoard.myPosition != 0) LeaderBoard.richestEnemy() else null
            AutoTargetType.FRONT -> LeaderBoard.aheadOfMe { it.second == LeaderBoard.PlayerType.ENEMY }?.first
            AutoTargetType.BEHIND -> LeaderBoard.behindMe { it.second == LeaderBoard.PlayerType.ENEMY }?.first
            else -> if (type.useType == UseType.ATTACK)
                LeaderBoard.richerEnemy {
                    if (type.effectType == EffectType.PERSISTENT)
                        it.first.isNotAfflictedWith(type.itemName)
                    else true
                } ?: LeaderBoard.behindMe {
                    if (it.second == LeaderBoard.PlayerType.FRIEND)
                        false
                    else if (type.effectType == EffectType.PERSISTENT)
                        it.first.isNotAfflictedWith(type.itemName)
                    else true
                }?.first
            else ME
        } ?: return null

        if (!ignoreCurrentEffects && type.effectType == EffectType.PERSISTENT) {
            if (type.useType == UseType.BUFF && ((!Points.running && manualTarget == null) || ME.isAfflictedWith(type.itemName)))
                return null
            if (type.useType == UseType.ATTACK && target.isAfflictedWith(type.itemName))
                return null
        }

        return takeItem(type.itemName)?.let { it to target }
    }

    private fun selectItem(): Pair<Item, String>? {
        if (_strategies["atTheTop"] ?: false) {
            val currentTime = System.currentTimeMillis()
            return if ((currentTime - (_itemsLastUsed["Gold Ring"] ?: 0)) > 180000L)
                findItem("Gold Ring", ignoreCurrentEffects = true)
            else if ((currentTime - (_itemsLastUsed["Star"] ?: 0)) > 300000L)
                findItem("Star", ignoreCurrentEffects = true)
            else if ((currentTime - (_itemsLastUsed["Morger Beard"] ?: 0)) > 900000L)
                findItem("Morger Beard", ignoreCurrentEffects = true)
            else {
                val behind = LeaderBoard.behindMe()
                if (behind != null
                        && behind.second == LeaderBoard.PlayerType.ENEMY
                        && LeaderBoard.pointsByName(behind.first) >= oneTrillionPoints)
                    findItem("Holy Water", manualTarget = behind.first)
                else null
            }
        }

        if ((_strategies["forTheWin"] ?: false) && Points.points > targetWinningPoints) {
            val myEffects = LeaderBoard.myEffects

            val hasStar = "Star" in myEffects
            val hasGoldRing = "Gold Ring" in myEffects
            val hasBeard = "Morger Beard" in myEffects

            return if (!hasStar)
                findItem("Star")
            else if (!hasGoldRing)
                findItem("Gold Ring")
            else if (!hasBeard)
                findItem("Morger Beard")
            else null
        }

        val selected = if (Points.running) when (LeaderBoard.myPosition) {
            in 0..1 -> selectDefensiveItem()
            in 2..9 -> findItem("Mushroom")
            in 10..15 -> selectGainItem()
            else -> selectJumpItem()
        } else null

        return selected ?: selectTreasureItem() ?: selectOffensiveItem() ?: selectCommonItem()
    }

    private fun selectDefensiveItem() = if (_strategies["defensive"] ?: true) findOneOfItem(arrayOf(
            "Gold Ring",
            "Star",
            "Morger Beard"
    )) else null

    private fun selectOffensiveItem() = if (_strategies["offensive"] ?: true) findOneOfItem(arrayOf(
            "Banana Peel",
            "Green Shell",
            "Fire Flower",
            "Charizard",
            "Hard Knuckle",
            "SPNKR",
            "Buster Sword",
            "Rail Gun",
            "Portal Nun",
            "Fus Ro Dah",
            "Holy Water"
    )) else null

    private fun selectTreasureItem() = if ((_strategies["treasure"] ?: true) && "Poroggo" !in LeaderBoard.myBadges) findOneOfItem(arrayOf(
            "Pokeball",
            "Treasure Chest",
            "Da Da Da Da Daaa Da DAA da da"
    )) else null

    private fun selectGainItem() = if (_strategies["gain"] ?: true) findOneOfItem(arrayOf(
            "Moogle",
            "Warthog",
            "Rush the Dog"
    )) else null

    private fun selectJumpItem() = if (_strategies["jump"] ?: true) findRandomOneOfItem(arrayOf(
            "Bullet Bill",
            "Morph Ball",
            "7777"
    )) else null

    private fun selectCommonItem() = if (_strategies["common"] ?: true) {
        if (LeaderBoard.isWerewolf && LocalDateTime.now().hour == 0) findRandomOneOfItem(arrayOf(
                "UUDDLRLRBA",
                "Biggs",
                "Wedge",
                "Bo Jackson",
                "Pizza",
                "Buffalo"
        )) else findRandomOneOfItem(arrayOf(
                "Banana Peel",
                "Green Shell",
                "Fire Flower",
                "Charizard",
                "Hard Knuckle",
                "Crowbar",
                "SPNKR",
                "Red Shell",
                "Buster Sword",
                "Rail Gun",
                "Portal Nun",
                "Fus Ro Dah",
                "Holy Water",
                "Hadouken",
                "UUDDLRLRBA",
                "Biggs",
                "Wedge",
                "Bo Jackson",
                "Pizza",
                "Buffalo"
        ))
    } else null

    private fun loadItemTypes(): List<ItemType> {
        return using {
            val stmt = dbConnection.prepareStatement(LOAD_TYPE_SQL).autoClose()
            val results = stmt.executeQuery().autoClose()

            val types = mutableListOf<ItemType>()
            while (results.next()) {
                types.add(ItemType(
                        results.getString(1),
                        results.getString(2),
                        results.getInt(3),
                        UseType.valueOf(results.getString(4)),
                        EffectType.valueOf(results.getString(5)),
                        AutoTargetType.valueOf(results.getString(6))
                ))
            }
            types
        }
    }

    private fun loadItems(): List<Item> {
        return using {
            val stmt = dbConnection.prepareStatement(LOAD_ITEM_SQL).autoClose()
            val results = stmt.executeQuery().autoClose()

            val items = mutableListOf<Item>()
            while (results.next()) {
                val name = results.getString(2)
                val type = _itemTypes[name]
                if (type != null) {
                    val item = Item(results.getString(1), type, results.getString(2))
                    items.add(item)
                }
            }
            items
        }
    }

    private fun Item.add(): Item = synchronized(_items) {
        this.apply { _items.getOrPut(type.itemName) { LinkedList<Item>() }.add(this) }
    }

    private fun Item.use(target: String, apiKey: String? = null): List<String>? {
        remove()
        return RequestHandler.forKey(apiKey).sendMessage("$ITEMS_URL/$id?target=$target", HttpMethod.POST).get()?.let {
            val used = parseJson<ItemUseModel>(it) ?: return null
            with(used) {
                messages.map { bonusItemMessageRegex.matchEntire(it.trim()) }
                        .filterNotNull()
                        .forEach {
                            val id = it.groupValues[1]
                            val name = it.groupValues[2]
                            val bonus = ItemModel(id, name, 0, "bonus item")
                            acquireItem(bonus, "bonus")
                        }
            }

            _usedItems.boundedAdd(ItemUsedEvent(type.itemName, target))
            _itemsLastUsed.put(type.itemName, System.currentTimeMillis())

            used.messages
        }
    }

    private fun ItemType.save() = try {
        _itemTypes.put(itemName, this)
        using {
            val stmt = dbConnection.prepareStatement(STORE_TYPE_SQL).autoClose()
            stmt.setString(1, itemName)
            stmt.setString(2, itemDescription)
            stmt.setInt(3, itemRarity)
            stmt.executeUpdate()
        }
    } catch (e: Exception) {
        mysqlLogger.error("could not store a new item type - $this", e)
    }

    private fun Item.save() = try {
        add()
        using {
            val stmt = dbConnection.prepareStatement(STORE_ITEM_SQL).autoClose()
            stmt.setString(1, id)
            stmt.setString(2, type.itemName)
            stmt.setString(3, source)
            stmt.setTimestamp(4, Timestamp(System.currentTimeMillis()))
            stmt.executeUpdate()
        }
    } catch (e: Exception) {
        mysqlLogger.error("could not store a new item - $this", e)
    }

    private fun Item.remove() = try {
        using {
            val stmt = dbConnection.prepareStatement(REMOVE_ITEM_SQL).autoClose()
            stmt.setTimestamp(1, Timestamp(System.currentTimeMillis()))
            stmt.setString(2, id)
            stmt.executeUpdate()
        }
    } catch (e: Exception) {
        mysqlLogger.error("could not remove an item - $this", e)
    }

    data class ItemAcquiredEvent(
            val type: String,
            val source: String,
            val timestamp: String = LocalDateTime.now().toString()
    )

    data class ItemUsedEvent(
            val type: String,
            val target: String,
            val timestamp: String = LocalDateTime.now().toString()
    )

    private const val LOAD_TYPE_SQL = "select item_name, item_description, item_rarity, use_type, effect_type, auto_target_type from item_types"
    private const val LOAD_ITEM_SQL = "select id, `type`, source from items where used_on is null order by received_on"
    private const val STORE_TYPE_SQL = "insert into item_types (item_name, item_description, item_rarity) values (?, ?, ?)"
    private const val STORE_ITEM_SQL = "insert into items (id, `type`, source, received_on) values (?, ?, ?, ?)"
    private const val REMOVE_ITEM_SQL = "update items set used_on = ? where id = ?"
}