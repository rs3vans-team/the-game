package com.nerdery.revans.thegame

import org.slf4j.LoggerFactory

val threadLogger = LoggerFactory.getLogger("thread")
val httpLogger = LoggerFactory.getLogger("thegame")
val jsonLogger = LoggerFactory.getLogger("json")
val mysqlLogger = LoggerFactory.getLogger("mysql")
