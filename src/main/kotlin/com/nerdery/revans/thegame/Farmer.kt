package com.nerdery.revans.thegame

open class Farmer(val source: String,
                  val apiKey: String?) : TheGameThread() {

    override fun timeToWait(timeWorked: Long, failure: Boolean): Long = 1001L

    override fun tick() {
        executeRequestAndCaptureItem()
    }

    protected fun executeRequestAndCaptureItem(): PointsModel? {
        val model = RequestHandler.forKey(apiKey).sendMessage(POINTS_URL, HttpMethod.POST).get()?.let { parseJson<PointsModel>(it) } ?: return null

        if (model.item != null)
            Items.acquireItem(model.item.wrapped.first(), source)

        return model
    }

    companion object {

        val active: Map<String, Farmer>
            get() = farmers

        private val farmers = mutableMapOf<String, Farmer>()

        fun startAll() {
            otherApiKeys.forEach { start(it.key, it.value) }
        }

        fun start(name: String): Boolean {
            val apiKey = otherApiKeys[name] ?: return false
            return if (!farmers.containsKey(name))
                start(name, apiKey)
            else false
        }

        private fun start(name: String, apiKey: String): Boolean =
                Farmer(name, apiKey)
                        .apply { farmers.put(name, this) }
                        .start()

        fun stopAll() {
            otherApiKeys.forEach { stop(it.key) }
        }

        fun stop(name: String): Boolean = farmers.remove(name)?.stop() ?: false
    }
}