package com.nerdery.revans.thegame

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.mashape.unirest.http.Unirest
import com.mashape.unirest.request.HttpRequest

val mapper = jacksonObjectMapper()

private fun createRequest(url: String, method: HttpMethod): HttpRequest {
    val request = if (method == HttpMethod.GET) {
        Unirest.get(url)
    } else if (method == HttpMethod.POST) {
        Unirest.post(url)
    } else {
        throw UnsupportedOperationException("Unexpected http method: $method")
    }
    return request
            .header("Content-Type", "application/json")
            .header("Accept", "application/json")
}

private fun createAuthedRequest(url: String, method: HttpMethod, apiKey: String? = null): HttpRequest {
    return createRequest(url, method)
            .header("apikey", apiKey ?: myApiKey)
}

fun executeHttpRequest(url: String, method: HttpMethod): String? =
        executeHttpRequest(createRequest(url, method))

fun executeAuthedHttpRequest(url: String, method: HttpMethod, apiKey: String? = null): String? =
        executeHttpRequest(createAuthedRequest(url, method, apiKey))

private fun executeHttpRequest(request: HttpRequest): String? = try {
    val response = request.asString()

    if (response.status >= 400)
        throw IllegalStateException("Invalid HTTP response code: ${response.status} (${response.statusText})\n${response.body ?: "<nothing>"}")

    response.body?.apply { jsonLogger.trace(this) }
} catch (e: Exception) {
    httpLogger.error("Error performing HTTP request: ${request.url} (${request.headers})", e)
    null
}

inline fun <reified T : Any> parseJson(json: String): T? {
    return try {
        mapper.readValue<T>(json)
    } catch (e: Exception) {
        httpLogger.error("Error performing JSON parse:\n$json", e)
        return null
    }
}

inline fun <reified T : Any> executeHttpRequestAndParseJson(url: String, method: HttpMethod): T? {
    return parseJson(executeHttpRequest(url, method) ?: return null)
}

inline fun <reified T : Any> executeAuthedHttpRequestAndParseJson(url: String, method: HttpMethod, apiKey: String? = null): T? {
    return parseJson(executeAuthedHttpRequest(url, method, apiKey) ?: return null)
}

enum class HttpMethod {
    GET,
    POST
}