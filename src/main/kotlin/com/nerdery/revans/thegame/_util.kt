package com.nerdery.revans.thegame

import java.math.BigDecimal
import java.math.BigInteger
import java.util.*

private val random = Random()

fun <T> Array<T>.shuffle(): Array<T> {
    val copy = this.copyOf()
    for (i in 0..copy.size - 1) {
        val r = random.nextInt(copy.size)
        val tmp = copy[i]
        copy[i] = copy[r]
        copy[r] = tmp
    }
    return copy
}


// Kotlin doesn't support [use] on AutoCloseable yet... :(

class ResourceHolder : AutoCloseable {
    val resources = arrayListOf<AutoCloseable>()

    fun <T : AutoCloseable> T.autoClose(): T {
        resources.add(this)
        return this
    }

    override fun close() {
        resources.reversed().forEach { it.close() }
    }
}

fun <R> using(block: ResourceHolder.() -> R): R {
    val holder = ResourceHolder()
    try {
        return holder.block()
    } finally {
        holder.close()
    }
}


class BoundedLinkedList<T>(val bound: Int) : LinkedList<T>() {

    fun boundedAdd(t: T) {
        addFirst(t)
        trim()
    }

    fun boundedAddAll(list: List<T>) {
        addAll(0, list)
        trim()
    }

    private fun trim() {
        while (size > bound)
            removeLast()
    }
}


fun Int.toBigInt() = BigInteger.valueOf(this.toLong())!!
fun String.toBigInt() = BigInteger(this)
fun BigDecimal.toBigInt(): BigInteger = this.setScale(0, BigDecimal.ROUND_HALF_UP).toBigInteger()
fun BigInteger.toBigDec() = BigDecimal(this)