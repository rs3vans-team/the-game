package com.nerdery.revans.thegame

import java.math.BigInteger
import java.time.LocalDateTime
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock

object Points : TheGameThread() {

    var points: BigInteger = BigInteger.ZERO
        private set
    val pointEvents: List<PointEvent>
        get() = _pointEvents.toList()
    val messages: List<String>
        get() = _messages.toList()

    private val _pointEvents = BoundedLinkedList<PointEvent>(50)
    private val _messages = BoundedLinkedList<String>(50)

    private val lock = ReentrantLock()

    override fun timeToWait(timeWorked: Long, failure: Boolean): Long = if (noMorePoints()) 300L else 1001L

    override fun tick() = lock.withLock {
        if (points == BigInteger.ZERO || noMorePoints()) {
            checkPointsAndEffects()
            if (points == maxPointsAllowed)
                return
        }

        val model = RequestHandler.sendMessage(POINTS_URL, HttpMethod.POST).get()?.let {
            parseJson<PointsModel>(it)
        } ?: return

        val totalPoints = model.points.toBigInt()
        if (points == BigInteger.ZERO) {
            points = totalPoints
        } else {
            val pointsChange = totalPoints - points
            if (pointsChange != BigInteger.ZERO) {
                if (pointsChange.abs() > BigInteger.valueOf(10000)) {
                    _pointEvents.boundedAdd(PointEvent(
                            "significant change",
                            pointChange = pointsChange
                    ))
                }

                points = totalPoints
            }
        }

        if (totalPoints == maxPointsAllowed
                || ("Hadouken" in model.effects && totalPoints == (maxPointsAllowed - BigInteger.ONE))) {
            setAtTheTopStrategy()
        } else if (totalPoints > targetWinningPoints && !Items.strategyIsActive("forTheWin")) {
            Items.activateStrategy("forTheWin", true)
            _pointEvents.boundedAdd(PointEvent("GO FOR THE WIN"))
        }

        LeaderBoard.updateEffectsForName(ME, model.effects)

        if (model.item != null)
            Items.acquireItem(model.item.wrapped.first(), ME)

        if (model.messages.isNotEmpty())
            _messages.boundedAddAll(model.messages)
    }

    fun checkPointsAndEffects() = lock.withLock {
        val model = RequestHandler.sendMessage(POINTS_URL, HttpMethod.GET).get()?.let {
            parseJson<WrappedPlayerModel>(it)?.wrapped?.first()
        } ?: return@withLock

        points = model.points.toBigInt()
        LeaderBoard.updateEffectsForName(model.playerName, model.effects)

        if (points == maxPointsAllowed)
            setAtTheTopStrategy()

        if (model.effects.isEmpty() && Items.strategyIsActive("atTheTop"))
            Items.queueItem("Gold Ring", target = ME)
    }

    private fun noMorePoints() = points == maxPointsAllowed || Items.strategyIsActive("atTheTop")

    private fun setAtTheTopStrategy() {
        Items.activateStrategy("atTheTop", true)
        _pointEvents.boundedAdd(PointEvent("AT THE TOP"))
    }

    data class PointEvent(
            val type: String,
            val timestamp: String = LocalDateTime.now().toString(),
            val pointChange: BigInteger? = null
    )
}