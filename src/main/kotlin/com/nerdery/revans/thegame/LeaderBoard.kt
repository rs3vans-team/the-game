package com.nerdery.revans.thegame

import java.math.BigInteger
import java.util.*
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock

object LeaderBoard : TheGameThread() {

    val myEffects: Set<String>
        get() = effectsByName(ME) ?: emptySet()
    val myBadges: Set<String>
        get() = badgesByName(ME) ?: emptySet()
    val myTitle: String?
        get() = titleByName(ME)
    val myPosition: Int
        get() = positionByName(ME) ?: 0

    val isWerewolf: Boolean
        get() = "Werewolf" in myBadges
    val isVampire: Boolean
        get() = "Vampire" in myBadges

    val points: List<Pair<String, BigInteger>>
        get() = lock.withLock { playersByPoints.descendingMap().map { it.value.first to it.key } }

    val friends = setOf(ME, "rvanbelk", "myoung", "zbrown", "johuff", "nscott", "ppurcell")

    val enemies: List<String>
        get() = lock.withLock {
            playersByPoints
                    .descendingMap()
                    .map { it.value.first }
                    .filter { it !in friends }
                    .toList()
        }

    val unguardedEnemies: Map<String, BigInteger>
        get() = lock.withLock { HashMap(unguardedByPlayer) }

    private val players = mutableMapOf<String, Pair<BigInteger, PlayerType>>()
    private val playersByPoints = TreeMap<BigInteger, Pair<String, PlayerType>>()
    private val effectsByPlayer = mutableMapOf<String, Set<String>>()
    private val badgesByPlayer = mutableMapOf<String, Set<String>>()
    private val titlesByPlayer = mutableMapOf<String, String>()
    private val unguardedByPlayer = mutableMapOf<String, BigInteger>()

    private val lock = ReentrantLock()

    override fun timeToWait(timeWorked: Long, failure: Boolean): Long = 10000L

    override fun tick() = lock.withLock {
        val allHeroes = generateSequence(0) { it + 1 }
                .map { requestLeaderBoardPage(it) }
                .takeWhile { it.isNotEmpty() }
                .flatten()

        playersByPoints.clear()
        unguardedByPlayer.clear()

        allHeroes.forEach {
            val playerName = it.playerName
            val playerType = if (playerName in friends) PlayerType.FRIEND else PlayerType.ENEMY
            val playerPoints = it.points.toBigInt()
            val playerEffects = it.effects
            val playerBadges = it.badges
            val playerTitle = it.title

            players.put(playerName, playerPoints to playerType)
            playersByPoints.put(playerPoints, playerName to playerType)
            effectsByPlayer.put(playerName, playerEffects)
            badgesByPlayer.put(playerName, playerBadges.map { it.name }.toSet())

            if (playerTitle != null)
                titlesByPlayer.put(playerName, playerTitle)
            else
                titlesByPlayer.remove(playerName)

            if (playerType == PlayerType.ENEMY && "Carbuncle" in playerEffects
                    && !("Gold Ring" in playerEffects || "Star" in playerEffects
                    || "Tanooki Suit" in playerEffects || "Varia Suit" in playerEffects)) {
                unguardedByPlayer.put(playerName, playerPoints)
            }
        }

        ItemSequences.triggerAutomaticItemSequences()
    }

    private fun requestLeaderBoardPage(pageNum: Int): List<PlayerModel> =
            executeHttpRequestAndParseJson<List<PlayerModel>>("$BASE_URL?page=$pageNum", HttpMethod.GET) ?: emptyList()

    fun pointsByName(name: String): BigInteger = players[name]?.first ?: BigInteger.ZERO

    fun effectsByName(name: String): Set<String>? = lock.withLock { effectsByPlayer[name] }
    fun String.isAfflictedWith(effect: String) = lock.withLock { effectsByPlayer[this]?.contains(effect) ?: false }
    fun String.isNotAfflictedWith(effect: String) = !this.isAfflictedWith(effect)

    fun updateEffectsForName(name: String, effects: Set<String>): Boolean = lock.withLock { effects == effectsByPlayer.put(name, effects) }

    fun badgesByName(name: String): Set<String>? = lock.withLock { badgesByPlayer[name] }

    fun titleByName(name: String): String? = lock.withLock { titlesByPlayer[name] }

    fun richestEnemy(filter: ((Pair<String, BigInteger>) -> Boolean)? = null): String? = lock.withLock {
        playersByPoints
                .descendingMap()
                .filterValues { it.second == PlayerType.ENEMY }
                .filter { filter?.invoke(it.value.first to it.key) ?: true }
                .values
                .firstOrNull()
                ?.first
    }

    fun richerEnemy(filter: ((Pair<String, BigInteger>) -> Boolean)? = null): String? = lock.withLock {
        players[ME]
                ?.let { playersByPoints.tailMap(it.first, false) }
                ?.entries
                ?.filter { it.value.second == PlayerType.ENEMY }
                ?.filter { filter?.invoke(it.value.first to it.key) ?: true }
                ?.map { it.value.first }
                ?.toTypedArray()
                ?.shuffle()
                ?.firstOrNull()
    }

    fun aheadOfMe(filter: ((Pair<String, PlayerType>) -> Boolean)? = null): Pair<String, PlayerType>? = lock.withLock {
        players[ME]
                ?.let { playersByPoints.tailMap(it.first, false) }
                ?.values
                ?.firstOrNull { filter?.invoke(it) ?: true }
    }

    fun behindMe(filter: ((Pair<String, PlayerType>) -> Boolean)? = null): Pair<String, PlayerType>? = lock.withLock {
        players[ME]
                ?.let { playersByPoints.headMap(it.first, false) }
                ?.values
                ?.lastOrNull { filter?.invoke(it) ?: true }
    }

    fun positionByName(name: String): Int? = lock.withLock {
        players[name]?.let { playersByPoints.tailMap(it.first, false) }?.size?.plus(1)
    }

    enum class PlayerType {
        FRIEND,
        ENEMY
    }
}