# THE GAME #

A Kotlin client which plays _the Game_.
 
### Features ###

* Points: continually POSTs to the `/points` endpoint, collecting points and items.
* Items: stores and manages items. Uses MySQL to persist items.
* ItemSequences: contains automated actions that try to do cool stuff with items!
* LeaderBoard: Retrieves and organizes information about all the players.
* Events: Retrieves and organizes item events that occur.
* Farmer: Uses an alternate API key to farm for items.
* Attacker: Uses an alternate API key to attack with items.

### Configuration ###

* API key: Your API key must be placed in a file in the `./private/apiKey` file.
* Database: Database connection properties must be placed in the `./private/db.properties` file. The following properties are required:
** `db.url` - the JDBC URL for connecting to the database
** `db.driver` - the JDBC driver class
** `db.user` - the user needed to connect to the database
** `db.password` - the password needed to connect to the database
* Item farmer/attacker API keys: Any item farmer/attacker keys must be placed, each on its own line, in the `./private/otherApiKeys` file. Each line must take the format of `<label>|<API key>`.

### Running ###

The client can be run in the foreground by executing the following:

    ./gradlew run

Additionally, the client can be run in the background (daemon) using the following (uses nohup):

    ./run.sh

### Interaction ###

The client can be queried/controlled using any REST client. The following endpoints are available:

#### Threads ####
* `GET /threads` - List threads and their status
* `PUT /threads/{thread}/start` - Start a stopped normal thread
* `PUT /threads/{thread}/stop` - Stop a running normal thread

#### "Me" Status ####
* `GET /me` - Return the following information about the current player:
** `points` - current total points
** `title` - current title
** `effects` - current effects
** `badges` - current badges
** `pointEvents` - interesting events concerning player's points
** `recentlyUsedItems` - list of recently used items
** `recentlyAcquiredItems` - list of recently acquired items

#### Messages ####
* `GET /messages` - List any interesting messages that have been received (most recent 1000)

#### Points ####
* `GET /points` - List all players and their points (highest to lowest)

#### Events ####
* `GET /events` - List all recent events
* `GET /events/me` - List all recent events pertaining to current player
* `GET /events/{player}` - List all recent events pertaining to specified player

#### Items ####
* `GET /items` - List all items and the count available
* `GET /items/strategies` - List any set configurable item strategies
* `PUT /items/strategies/{strategy}?value={value}` - Set an item strategy
* `GET /items/types` - List all known item types
* `GET /items/lastUsed` - List the time each item was last used
* `POST /items/take/{item}` - Return the ID of an available item of the given type, and mark it as "used"
* `GET /items/queue` - List any queued items
* `POST /items/queue/{item}[?target={target}&times={times}] - Queue an item of the given type (optionally targeted to the given target, and for the given number of times)
* `DELETE /items/queue` - Clear any queued items
* `POST /items/fire/{item}[?target={target}&times={times}&spread={true|false}&top={top}] - Immediately use an item of the given type by utilizing extra API keys (optionally targeted to the given target, and for the given number of times)
* `POST /items/reload` - Reload unused items from the database